При равных частотах

Кодирование

0) инициализация
D = 3, C = [1 1 2], B = [], N = 1024, l = 0, t = N, rb = 0, i = 0, k = 0, H = [0, N/4, N/2, 3*N/4, N]

1) чтение цифры (для перевода D→2, то есть для равных частот; для сжатия будут другие формулы пересчёта l и t)
i=i+1, ci = C(i), d = t-l; t = floor(l + d*(ci+1)/D); l = floor(l + d*ci/D); lt=[l t], d = t-l, H

2) масштабирование и запись бита (верно и для сжатия тоже) — может быть несколько раз, пока не станет d > N/2 

bk = 0, L=bk*N/2, B = [B, bk, ones(1, rb)*(1-bk)], k=k+1+rb, rb=0,  l=(l-L)*2;t=(t-L)*2; lt=[l t], d = t-l, H
bk = 1, L=bk*N/2, B = [B, bk, ones(1, rb)*(1-bk)], k=k+1+rb, rb=0,  l=(l-L)*2;t=(t-L)*2; lt=[l t], d = t-l, H

L=N/4, rb = rb+1,  l=(l-L)*2;t=(t-L)*2; lt=[l t], d = t-l, H

хвост формулы при L=N/4 (средняя половина) отличается от 0 и N/2!


3) завершение (при i=n, если масштабировать уже не можем)

B = [B, 1], k=k+1


Декодирование

0) инициализация (так как формула для промежуточных точек W полуинтервала [lc, tc) использует массив w (омега в презентации), она верна и для сжатия)
D = 3, w = 0:D, B = [1 0 0 0 1 0 0 0 0 0 0], C = [], N = 1024, lb = 0, tb = N, lc = 0, tc = N, i = 0, k = 0, H = [0, N/4, N/2, 3*N/4, N], dc = tc-lc; W = floor(lc + dc*w/D)

1) чтение бита (верно и для сжатия тоже)
k=k+1, bk = B(k), db = tb-lb; tb = floor(lb + db*(bk+1)/2); lb = floor(lb + db*bk/2); ltb=[lb tb], db = tb-lb, W

2) получение и запись цифры (для перевода D→2, то есть для равных частот; для сжатия будут другие формулы пересчёта lc, tc и промежуточных точек W)

ci = 0, C = [C, ci], i=i+1, dc = tc-lc; tc = floor(lc + dc*(ci+1)/D); lc = floor(lc + dc*ci/D); ltc=[lc tc], dc = tc-lc, H

ci = 1, C = [C, ci], i=i+1, dc = tc-lc; tc = floor(lc + dc*(ci+1)/D); lc = floor(lc + dc*ci/D); ltc=[lc tc], dc = tc-lc, H

ci = 2, C = [C, ci], i=i+1, dc = tc-lc; tc = floor(lc + dc*(ci+1)/D); lc = floor(lc + dc*ci/D); ltc=[lc tc], dc = tc-lc, H

3) масштабирование (верно и для сжатия тоже)

L=0,  lc=(lc-L)*2; lb=(lb-L)*2; tb=(tb-L)*2; tc=(tc-L)*2; ltb=[lb, tb], db=tb-lb, ltc=[lc tc], dc=tc-lc, W = floor(lc + dc*w/D)

L=N/4,  lc=(lc-L)*2; lb=(lb-L)*2; tb=(tb-L)*2; tc=(tc-L)*2; ltb=[lb, tb], db=tb-lb, ltc=[lc tc], dc=tc-lc, W = floor(lc + dc*w/D)

L=N/2,  lc=(lc-L)*2; lb=(lb-L)*2; tb=(tb-L)*2; tc=(tc-L)*2; ltb=[lb, tb], db=tb-lb, ltc=[lc tc], dc=tc-lc, W = floor(lc + dc*w/D)

хвосты формул все три одинаковые


При неравных частотах

алфавит A = {ξ1, ξ2,...ξT} отсортирован по убыванию частот (f — частоты), 
ci = ξj
i — порядковый номер символа в сообщении;
j — порядковый номер того же символа в алфавите A.



T — размер алфавита;
в презентации массив омег нумеруется с нуля до T, в Octave w неизбежно с 1 до T+1;
если j — номер в алфавите с единицы, то границы символа ξj [w(j), w(j+1))
(-)t = floor(l + d*w(j)/D); l = floor(l + d*w(j-1)/D)
(+)t = floor(l + d*w(j+1)/D); l = floor(l + d*w(j)/D)

при кодировании чтение символа

i=i+1, j = C(i), d = t-l; t = floor(l + d*w(j+1)/D); l = floor(l + d*w(j)/D); lt=[l t], d = t-l, H

при декодировании запись символа

j = 1, C = [C, ci], i=i+1, dc = tc-lc; tc = floor(lc + dc*w(j+1)/D); lc = floor(lc + dc*w(j)/D); ltc=[lc tc], dc = tc-lc, H


Кодирование

0) инициализация
C = «АНАНАС», алфавит A = {А, Н, С}

f = [3 2 1], T = length(f), w = [0 cumsum(f)], D = w(length(w)), C = [1 2 1 2 1 3], n = length(C), B = [], N = 1024, l = 0, t = N, rb = 0, i = 0, k = 0, H = [0, N/4, N/2, 3*N/4, N], d = t-l; W = floor(l + d*w/D) 

f — массив частот;
T — длина алфавита = длина массива частот;
w — массив накопленных частот ω[j+1] (то есть нумеруется с 1 до T+1);
D — делитель;
C — кодируемое сообщение;
n — длина кодируемого сообщения в символах;
B — выходной битовый поток (код сообщения);
N — длина целочисленного изображения (в лекциях так и есть N), не меняется в процессе расчётов;
l и t — границы (l — нижняя включаемая, t — верхняя невключаемая) целочисленного изображения рабочего ПИ;
rb — β, число зарезервированных, но неизвестных битов = число масштабирований из средней половины подряд перед текущим шагом;
i — номер прочитанного символа в сообщении (с единицы);
k — номер записанного бита в коде (с единицы; может меняться скачком на 1+β) — здесь не нужно;
H — границы половин для масштабирования, не меняется в процессе расчётов;
W — границы символов на [l t] (массив w, натянутый на ПИ [l t]: W(1)=l, W последнее равно t) — при кодировании тоже не нужен, на самом деле (новые l и t можно пересчитать по формулам), но с ним нагляднее.

1) чтение символа

i=i+1, j = C(i), d = t-l; t = floor(l + d*w(j+1)/D); l = floor(l + d*w(j)/D); lt=[l t], d = t-l, H

2) масштабирование и запись бита

bk = 0, L=bk*N/2, B = [B, bk, ones(1, rb)*(1-bk)], k=k+1+rb, rb=0,  l=(l-L)*2; t=(t-L)*2; lt=[l t], d = t-l, H
bk = 1, L=bk*N/2, B = [B, bk, ones(1, rb)*(1-bk)], k=k+1+rb, rb=0,  l=(l-L)*2; t=(t-L)*2; lt=[l t], d = t-l, H

L=N/4, rb = rb+1,  l=(l-L)*2; t=(t-L)*2; lt=[l t], d = t-l, H

хвост формулы при L=N/4 (средняя половина) отличается от 0 и N/2! нет записи бита


3) завершение (при i=n, если масштабировать уже не можем)

B = [B, 1], k=k+1



Декодирование

0) инициализация (так как формула для промежуточных точек W полуинтервала [lc, tc) использует массив w (омега в презентации), она верна и для сжатия)
f = [3 2 1], T = length(f), w = [0 cumsum(f)], D = w(length(w)), B = [0 1 0 0 1 1 1 0 0 0 0 0], n, C = [], N = 1024, lb = 0, tb = N, lc = 0, tc = N, i = 0, k = 0, H = [0, N/4, N/2, 3*N/4, N], dc = tc-lc; W = floor(lc + dc*w/D)

если не очищаем память: f, T, w, D, N, а также B и n остаются; H, в принципе, тоже

B = [B zeros(1, 12)], n, C = [], lb = 0, tb = N, lc = 0, tc = N, i = 0, k = 0, H, dc = tc-lc; W = floor(lc + dc*w/D)

lb и tb — это λ и τ, границы бита;    db = δ = τ-λ;
lc и tc — это l и t, границы символа; dc = ∆ = t-l;
всё остальное как при кодировании;
W нужен: смотрим, в какой промежуток попадает [lb, tb);
i нужен: сравниваем с n; k — не очень, но пусть для наглядности будет;
β=rb не используется; масштабирование из «средней половины» ничем не отличается от нижней/верхней;
последовательность lc и tc должна совпадать с последовательностью l и t при кодировании (если не совпадает — скорее всего ошибочен порядок полушагов).


1) чтение бита
k=k+1, bk = B(k), db = tb-lb; tb = floor(lb + db*(bk+1)/2); lb = floor(lb + db*bk/2); ltb=[lb tb], db = tb-lb, W

2) получение и запись символа с номером в алфавите j (1...T)

j = 1, C = [C, j], i=i+1, dc = tc-lc; tc = floor(lc + dc*w(j+1)/D); lc = floor(lc + dc*w(j)/D); ltc=[lc tc], dc = tc-lc, H

3) масштабирование 

L=0,  lc=(lc-L)*2; lb=(lb-L)*2; tb=(tb-L)*2; tc=(tc-L)*2; ltb=[lb, tb], db=tb-lb, ltc=[lc tc], dc=tc-lc, W = floor(lc + dc*w/D)

L=N/4,  lc=(lc-L)*2; lb=(lb-L)*2; tb=(tb-L)*2; tc=(tc-L)*2; ltb=[lb, tb], db=tb-lb, ltc=[lc tc], dc=tc-lc, W = floor(lc + dc*w/D)

L=N/2,  lc=(lc-L)*2; lb=(lb-L)*2; tb=(tb-L)*2; tc=(tc-L)*2; ltb=[lb, tb], db=tb-lb, ltc=[lc tc], dc=tc-lc, W = floor(lc + dc*w/D)

после одного (2) → к (3);
после нескольких (3) → возврат к (2), и только если нельзя — переход к (1)

После чтения всех существующих битов можно читать нули по одному, а можно сразу пачкой (l не изменится, t будет равно l+1); далее можно пересчитывать только l: t=l+1 всегда (нули до бесконечности).
